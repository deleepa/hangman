/**
 * Authour: Deleepa
 * Date:    07/06/2013
 * Project: Hangman  |  addNewWordService
 */

package service;

import utils.WriteToFile;

public class AddNewWordService {
    
    public AddNewWordService(){
        
    }
    
    public boolean addWord(String newWord) {
                
        try {
            String path = "dictionary.txt";
            String[] writeData = {newWord};
            System.out.println("writeData.length " + writeData.length);
            WriteToFile writer = new WriteToFile();
            writer.appendFile(path, writeData);
            return true;
            
        } catch(Exception e) {
            
            System.out.println("caught exception in service");
            return false;
        }
        
    }
    
//    public static void main(String[] args) {
//        AddNewWordService serviceCall = new AddNewWordService("awesome");
//    }
}
