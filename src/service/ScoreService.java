/**
 * Authour: Deleepa
 * Date:    07/06/2013
 * Project: Hangman  |  SaveScoreService
 */

package service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import utils.ReadFile;
import utils.WriteToFile;

/*
 * The score service class performs many functions that are 
 * primarily involved with processing and saving the 
 * player's scores.
 * 
 *  1. One function this method has is to read the 
 *     current highscores and return a hashmap containing 
 *     some important information.
 *  2. The second function is to save the highscores.
 *  3. The third function is to sort the highscores.
 */

public class ScoreService {
    
    ReadFile reader = new ReadFile();
    WriteToFile writer = new WriteToFile();
    String scoresPath = "scores.txt";
    
    public ScoreService() {
        
    }
    
    /*
     * This method performs the first function. It reads the 
     * current highscores from the txt file and returns:
     *      - the current highest score
     *      - the current lowest score
     *      - the total number of scores
     * as a hashmap that will be decoded in the calling class.
     */
    public HashMap<String, Integer> readScores(){
        ArrayList<String> scores = new ArrayList<String>();
        HashMap<String, Integer> info =  new HashMap<String, Integer>();
        
        scores = reader.readFromFile(scoresPath);
        scores.removeAll(Collections.singleton(null));
        System.out.println("scores.size() " + scores.size());
        if(scores.size() == 0 || scores.size() == 1) {
            info.put("highestScore", null);
            info.put("lowestScore", null);
            info.put("numberOfScores", null);
            
            return info;
            
        } else {
            info.put("highestScore", Integer.parseInt(scores.get(0).substring(0, 2)));
            info.put("lowestScore", Integer.parseInt(scores.get(scores.size() - 1).substring(0, 2)));
            info.put("numberOfScores", scores.size());
        
        return info; 
        }
        
        
    }
    
    /*
     * This method performs the second function. It reads the current highscores,
     * sorts the scores and then writes those scores into the file. 
     */
    public boolean writeScore(String score, boolean overwrite){
        
        ArrayList<String> allScores = reader.readFromFile(scoresPath);
        allScores.removeAll(Collections.singleton(null)); 
        
        if(!overwrite) {
            try {
                String[] outputArray = new String[allScores.size()];
                allScores.add(score);
                System.out.println("allScores.size() " + allScores.size());
                outputArray = sort(allScores);
                writer.truncateAndWriteFile(scoresPath, outputArray);
            } catch(Exception e) {
                System.err.println(ScoreService.class.toString() + ".writeScore. Error: " + e);
            }
            
            return true;
            
        } else if(overwrite) {
            try {
                System.out.println("else if overwrite");
                String[] outputArray = new String[allScores.size()];
                allScores.set(allScores.size() - 1, score);
                outputArray = sort(allScores);
                writer.truncateAndWriteFile(scoresPath, outputArray);
            } catch(Exception e) {
                System.err.println(ScoreService.class.toString() + ".writeScore. Error: " + e);
            }
            
            return true;
            
            
        } else {
            
            return false;
        }     
    }
    
    /*
     * This method performs a sort on an arraylist that is passed and 
     * returns a string array. The arraylist must contain strings in a particular
     * format: "<score>_<player name>"
     */
    public String[] sort(ArrayList<String> temp){
        System.out.println("running sort");
        temp.removeAll(Collections.singleton(null)); 
        boolean flag = false;
        String lower = "";
        String[] tempArray = new String[temp.size()];
        
        
        for(int k = 0; k < temp.size(); k++) {
            tempArray[k] = temp.get(k);
        }
        
        do {
            
            for(int i = 0; i < (tempArray.length - 1); i++) {
                
                flag = false;
                int x = Integer.parseInt(tempArray[i].substring(0, 2));
                int y = Integer.parseInt(tempArray[i + 1].substring(0, 2));
                System.out.println(x);
                System.out.println(y);
                if(y > x) {
                    flag = true;
                    lower = tempArray[i];
                    tempArray[i] = tempArray[i + 1];
                    tempArray[i + 1] = lower;
                    break;
                }           
                
                
            }
            
        } while(flag);
       
        return tempArray;
    }
    
    public static void main(String[] args) {
        ScoreService serviceCall = new ScoreService();
        
        //serviceCall.readScores();
        
        ArrayList<String> temp = new ArrayList<String>();
        
        temp.add("23_jack");
        //temp.add("30_john");
//        temp.add("45_paul");
//        temp.add("16_hing lui");
//        temp.add("09_claire");
//        temp.add("01_robson");
//        temp.add("56_gavin");
        
//        for(int i = 0; i < temp.size(); i++) {
//            System.out.println(temp.get(i));
//        }
        
        serviceCall.writeScore("35_Jack", true);
    }
}
