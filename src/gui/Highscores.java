 /*
 * Application: Hangman (Class: WriteToFile)
 * Authour:     Deleepa
 * Date:        1st May 2013
 */
package gui;

import java.awt.Dimension;
import java.awt.Font;
import utils.ReadFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import main.Hangman;
import utils.WriteToFile;

public class Highscores extends javax.swing.JFrame {
    
    ArrayList<String> highscores = new ArrayList<String>();
    String temp = "";
    String[] tempArray = new String[2];
    JLabel[] player;
    JLabel[] score;
    int index = 0;
    
    public Highscores() {
        initComponents();
        arrangeLayout();
        loadData();
    }
    
    public void arrangeLayout(){
        mainPanel.setLayout(null);
 
        Font defaultFont = new Font("Lucida Sans", 0, 24);
        Dimension playerDimension = new Dimension(100, 50);
        Dimension scoreDimension = new Dimension(50, 50);
        
        
        player1.setSize(playerDimension);
        player1.setFont(defaultFont);
        player1.setLocation(120, 100);
        score1.setSize(scoreDimension);
        score1.setFont(defaultFont);
        score1.setLocation(300, 100);
        
        player2.setSize(playerDimension);
        player2.setFont(defaultFont);
        player2.setLocation(120, 150);
        score2.setSize(scoreDimension);
        score2.setFont(defaultFont);
        score2.setLocation(300, 150);
        
        player3.setSize(playerDimension);
        player3.setFont(defaultFont);
        player3.setLocation(120, 200);
        score3.setSize(scoreDimension);
        score3.setFont(defaultFont);
        score3.setLocation(300, 200);
        
        player4.setSize(playerDimension);
        player4.setFont(defaultFont);
        player4.setLocation(120, 250);
        score4.setSize(scoreDimension);
        score4.setFont(defaultFont);
        score4.setLocation(300, 250);
        
        player5.setSize(playerDimension);
        player5.setFont(defaultFont);
        player5.setLocation(120, 300);
        score5.setSize(scoreDimension);
        score5.setFont(defaultFont);
        score5.setLocation(300, 300);
        
        doneButton.setFont(defaultFont);
        doneButton.setSize(new Dimension(100, 40));
        doneButton.setLocation(400, 300);
        //doneButton.setVisible(false);
        
        clearAllButton.setFont(defaultFont);
        clearAllButton.setSize(new Dimension(150, 40));
        clearAllButton.setLocation(400, 350);
        clearAllButton.setText("Clear All");
    }
    
    public void loadData(){
        ReadFile reader = new ReadFile();
        highscores = reader.readFromFile("scores.txt");
        highscores.removeAll(Collections.singleton(null));
        System.out.println("highscores: " + highscores.size());
        System.out.println("highscores(0): " + highscores.get(0));
        
        player = new JLabel[highscores.size()];
        score = new JLabel[highscores.size()];
        
        if(highscores.size() != 0 && !highscores.get(0).equals("")) {
            for(int i = 0; i <= (highscores.size() - 1); i++) {
            System.out.println("in for");
            System.out.println("i: " + i);

                if(i == 0) {
                    System.out.println("i == 0");
                    player[i] = player1;
                    score[i] = score1;
                    temp = highscores.get(i);
                    tempArray = temp.split("_");
                    score[i].setText(tempArray[0]);
                    player[i].setText(tempArray[1]);
                }
                else if(i == 1) {
                    player[i] = player2;
                    score[i] = score2;
                    temp = highscores.get(i);
                    tempArray = temp.split("_");
                    score[i].setText(tempArray[0]);
                    player[i].setText(tempArray[1]);
                }
                else if(i == 2) {
                    player[i] = player3;
                    score[i] = score3;
                    temp = highscores.get(i);
                    tempArray = temp.split("_");
                    score[i].setText(tempArray[0]);
                    player[i].setText(tempArray[1]);
                }
                else if(i == 3) {
                    player[i] = player4;
                    score[i] = score4;
                    temp = highscores.get(i);
                    tempArray = temp.split("_");
                    score[i].setText(tempArray[0]);
                    player[i].setText(tempArray[1]);
                }
                else if(i == 4) {
                    player[i] = player5;
                    score[i] = score5;
                    temp = highscores.get(i);
                    tempArray = temp.split("_");
                    score[i].setText(tempArray[0]);
                    player[i].setText(tempArray[1]);
                }
            }
        } else {
            
            JLabel nothingToDisplay = new JLabel("No scores saved to display!");
            
            nothingToDisplay.setFont(new Font("Lucida Sans", 0, 24));
            nothingToDisplay.setSize(new Dimension(400, 50));
            nothingToDisplay.setLocation(100, 100);
            
            mainPanel.add(nothingToDisplay);
            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        headerLabel = new javax.swing.JLabel();
        highscoreLabel = new javax.swing.JLabel();
        player1 = new javax.swing.JLabel();
        player2 = new javax.swing.JLabel();
        player3 = new javax.swing.JLabel();
        player4 = new javax.swing.JLabel();
        player5 = new javax.swing.JLabel();
        score1 = new javax.swing.JLabel();
        score2 = new javax.swing.JLabel();
        score3 = new javax.swing.JLabel();
        score4 = new javax.swing.JLabel();
        score5 = new javax.swing.JLabel();
        doneButton = new javax.swing.JButton();
        clearAllButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainPanel.setPreferredSize(new java.awt.Dimension(600, 400));

        headerLabel.setFont(new java.awt.Font("Cambria", 1, 36)); // NOI18N
        headerLabel.setText("Hangman Game (alpha)");

        highscoreLabel.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N
        highscoreLabel.setText("Highscores");

        player1.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        player2.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        player3.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        player4.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        player5.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        score1.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        score2.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        score3.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        score4.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        score5.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N

        doneButton.setFont(new java.awt.Font("Lucida Sans", 0, 24)); // NOI18N
        doneButton.setText("Done");
        doneButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doneButtonClicked(evt);
            }
        });

        clearAllButton.setText("jButton1");
        clearAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearAllButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(headerLabel))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(highscoreLabel)))
                .addContainerGap(200, Short.MAX_VALUE))
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGap(140, 140, 140)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(player2, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                    .addComponent(player1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(player3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(player4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(player5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(88, 88, 88)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(score2, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE))
                    .addComponent(score3, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(score4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(score5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(score1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(doneButton)
                    .addComponent(clearAllButton))
                .addGap(47, 47, 47))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(headerLabel)
                .addGap(18, 18, 18)
                .addComponent(highscoreLabel)
                .addGap(30, 30, 30)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(score1, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(player1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(score2, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(player2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(player3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(score3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(clearAllButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                        .addComponent(doneButton))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(score4, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                            .addComponent(player4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(player5, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(score5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(46, 46, 46))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void doneButtonClicked(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doneButtonClicked
        StartPage game = new StartPage();
        game.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_doneButtonClicked
    
    /*
     * This method removes all information from the text file that 
     * saves the scores. Then it closes and reloads the window. 
     */
    private void clearAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearAllButtonActionPerformed
        try {
            WriteToFile writer = new WriteToFile();
            String path = "scores.txt";
            String[] writeData = {""};
            writer.truncateAndWriteFile("scores.txt", writeData);
            Highscores highscores = new Highscores();
            highscores.setVisible(true);
            this.dispose();
            
        } catch (Exception ex) {
            System.err.println(Highscores.class.toString() + ".clearAllButtonActionPerformed(). Error: " + ex);
        }
        
    }//GEN-LAST:event_clearAllButtonActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Highscores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Highscores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Highscores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Highscores.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Highscores().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clearAllButton;
    private javax.swing.JButton doneButton;
    private javax.swing.JLabel headerLabel;
    private javax.swing.JLabel highscoreLabel;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel player1;
    private javax.swing.JLabel player2;
    private javax.swing.JLabel player3;
    private javax.swing.JLabel player4;
    private javax.swing.JLabel player5;
    private javax.swing.JLabel score1;
    private javax.swing.JLabel score2;
    private javax.swing.JLabel score3;
    private javax.swing.JLabel score4;
    private javax.swing.JLabel score5;
    // End of variables declaration//GEN-END:variables
}
