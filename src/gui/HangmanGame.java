/*
 * Application: Hangman
 * Authour:     Deleepa
 * Date:        24th March 2013
 */

package gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import service.ScoreService;
import utils.ReadFile;

public class HangmanGame extends javax.swing.JFrame {
    
    String compWord = "";
    StringBuffer outputLabelText = new StringBuffer("");
    int triesLeftInt = 6;
    int randomIndex;
    boolean correctLetter = false;

    Dimension headDimensions = new Dimension(50, 53);
    
    
    //constructor
    public HangmanGame() {
        
        
        //Color textField = new Color(125, 67, 125);
        //userInput.setBackground(textField);
        
        //String compWord = "drunk";
        
        ReadFile reader = new ReadFile();
        
        ArrayList<String> allWords = reader.readFromFile("dictionary.txt");
        allWords.removeAll(Collections.singleton(null)); 
//        for(int i = 0; i < allWords.size(); i++) {
//            System.out.println("from main " + allWords.get(i));
//        }
        System.out.println("allWords.size() " + allWords.size());
        randomIndex = (int) (Math.random() * allWords.size());
        System.out.println("randomIndex: " + randomIndex);
        
        //check if dictionary is empty
        if(randomIndex == 0 && allWords.size() == 0) {
            String message = "Your dictionary is empty! Try adding a new word.";
            JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.WARNING_MESSAGE);
        }
        else {
            if(randomIndex == 0) {
                randomIndex = allWords.size();
            }
            
            System.out.println("in else, random index = " + randomIndex);
            initComponents();
        
            mainPanel.setLayout(null);
            aButton.setLocation(10, 85);
            bButton.setLocation(60, 85);
            cButton.setLocation(110, 85);
            dButton.setLocation(160, 85);
            eButton.setLocation(210, 85);
            fButton.setLocation(260, 85);

            gButton.setLocation(10, 120);
            hButton.setLocation(60, 120);
            iButton.setLocation(110, 120);
            jButton.setLocation(160, 120);
            kButton.setLocation(210, 120);
            lButton.setLocation(260, 120);

            mButton.setLocation(10, 155);
            nButton.setLocation(60, 155);
            oButton.setLocation(110, 155);
            pButton.setLocation(160, 155);
            qButton.setLocation(210, 155);
            rButton.setLocation(260, 155);

            sButton.setLocation(10, 190);
            tButton.setLocation(60, 190);
            uButton.setLocation(110, 190);
            vButton.setLocation(160, 190);
            wButton.setLocation(210, 190);
            xButton.setLocation(260, 190);

            yButton.setLocation(10, 225);
            zButton.setLocation(60, 225);

            outputLabel.setLocation(10, 350);
            triesLeftLabel.setLocation(10, 400);
            jLayeredPane1.setLocation(350, 85);

            //set jpanel to transparent to allow the frame background to show through
            mainPanel.setOpaque(false);
            
            if(randomIndex == allWords.size()) {
                compWord = allWords.get(randomIndex - 1);
            }
            else {
                compWord = allWords.get(randomIndex);
            }          
            
            System.out.println("compWord: " + compWord);

            String temp = "";

            for(int i = 0; i < compWord.length(); i++) {
                temp += "_";

            }

            outputLabelText.replace(0, temp.length(), temp);
            outputLabel.setText(outputLabelText.toString());

            triesLeftLabel.setText("Tries left: " + triesLeftInt);
        }
        
 
    }
    
    //initComponents method
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backgroundLabel = new javax.swing.JLabel();
        mainPanel = new javax.swing.JPanel();
        headerLabel = new javax.swing.JLabel();
        outputLabel = new javax.swing.JLabel();
        aButton = new javax.swing.JButton();
        bButton = new javax.swing.JButton();
        cButton = new javax.swing.JButton();
        dButton = new javax.swing.JButton();
        eButton = new javax.swing.JButton();
        fButton = new javax.swing.JButton();
        gButton = new javax.swing.JButton();
        hButton = new javax.swing.JButton();
        iButton = new javax.swing.JButton();
        jButton = new javax.swing.JButton();
        kButton = new javax.swing.JButton();
        lButton = new javax.swing.JButton();
        mButton = new javax.swing.JButton();
        oButton = new javax.swing.JButton();
        nButton = new javax.swing.JButton();
        pButton = new javax.swing.JButton();
        qButton = new javax.swing.JButton();
        rButton = new javax.swing.JButton();
        sButton = new javax.swing.JButton();
        tButton = new javax.swing.JButton();
        uButton = new javax.swing.JButton();
        vButton = new javax.swing.JButton();
        xButton = new javax.swing.JButton();
        yButton = new javax.swing.JButton();
        zButton = new javax.swing.JButton();
        wButton = new javax.swing.JButton();
        triesLeftLabel = new javax.swing.JLabel();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        platformLabel = new javax.swing.JLabel();
        headLabel = new javax.swing.JLabel();
        bodyLabel = new javax.swing.JLabel();
        leftLegLabel = new javax.swing.JLabel();
        rightLegLabel = new javax.swing.JLabel();
        rightHandLabel = new javax.swing.JLabel();
        leftHandLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        optionsMenu = new javax.swing.JMenu();
        highscoresMenuItem = new javax.swing.JMenuItem();
        backMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 204, 51));
        setMinimumSize(new java.awt.Dimension(600, 400));
        setResizable(false);

        backgroundLabel.setPreferredSize(new java.awt.Dimension(600, 500));
        backgroundLabel.setIcon(new javax.swing.ImageIcon("background.png"));

        mainPanel.setMinimumSize(new java.awt.Dimension(600, 500));
        mainPanel.setPreferredSize(new java.awt.Dimension(600, 500));

        headerLabel.setFont(new java.awt.Font("Cambria", 1, 36)); // NOI18N
        headerLabel.setText("Hangman Game (alpha)");

        outputLabel.setFont(new java.awt.Font("Lucida Sans", 1, 24)); // NOI18N
        outputLabel.setText("________________");

        aButton.setText("A");
        aButton.setMaximumSize(null);
        aButton.setMinimumSize(null);
        aButton.setPreferredSize(new Dimension(40, 25));
        aButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        bButton.setText("B");
        bButton.setMaximumSize(new java.awt.Dimension(0, 0));
        bButton.setMinimumSize(new java.awt.Dimension(0, 0));
        bButton.setPreferredSize(new Dimension(40, 25));
        bButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        cButton.setText("C");
        cButton.setMaximumSize(new java.awt.Dimension(0, 0));
        cButton.setMinimumSize(new java.awt.Dimension(0, 0));
        cButton.setPreferredSize(new Dimension(40, 25));
        cButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        dButton.setText("D");
        dButton.setPreferredSize(new Dimension(40, 25));
        dButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        eButton.setText("E");
        eButton.setPreferredSize(new Dimension(40, 25));
        eButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        fButton.setText("F");
        fButton.setPreferredSize(new Dimension(40, 25));
        fButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        gButton.setText("G");
        gButton.setPreferredSize(new Dimension(40, 25));
        gButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        hButton.setText("H");
        hButton.setPreferredSize(new Dimension(40, 25));
        hButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        iButton.setText("I");
        iButton.setPreferredSize(new Dimension(40, 25));
        iButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        jButton.setText("J");
        jButton.setPreferredSize(new Dimension(40, 25));
        jButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        kButton.setText("K");
        kButton.setPreferredSize(new Dimension(40, 25));
        kButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        lButton.setText("L");
        lButton.setPreferredSize(new Dimension(40, 25));
        lButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        mButton.setText("M");
        mButton.setPreferredSize(new Dimension(40, 25));
        mButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        oButton.setText("O");
        oButton.setPreferredSize(new Dimension(40, 25));
        oButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        nButton.setText("N");
        nButton.setPreferredSize(new Dimension(40, 25));
        nButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        pButton.setText("P");
        pButton.setPreferredSize(new Dimension(40, 25));
        pButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        qButton.setText("Q");
        qButton.setPreferredSize(new Dimension(40, 25));
        qButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        rButton.setText("R");
        rButton.setPreferredSize(new Dimension(40, 25));
        rButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        sButton.setText("S");
        sButton.setPreferredSize(new Dimension(40, 25));
        sButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        tButton.setText("T");
        tButton.setPreferredSize(new Dimension(40, 25));
        tButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        uButton.setText("U");
        uButton.setPreferredSize(new Dimension(40, 25));
        uButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        vButton.setText("V");
        vButton.setPreferredSize(new Dimension(40, 25));
        vButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        xButton.setText("X");
        xButton.setPreferredSize(new Dimension(40, 25));
        xButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        yButton.setText("Y");
        yButton.setPreferredSize(new Dimension(40, 25));
        yButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        zButton.setText("Z");
        zButton.setPreferredSize(new Dimension(40, 25));
        zButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        wButton.setText("W");
        wButton.setPreferredSize(new Dimension(40, 25));
        wButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wordInput(evt);
            }
        });

        triesLeftLabel.setFont(new java.awt.Font("Lucida Sans", 1, 24)); // NOI18N
        triesLeftLabel.setText("Tries left:");
        triesLeftLabel.setMaximumSize(new java.awt.Dimension(0, 0));
        triesLeftLabel.setMinimumSize(new java.awt.Dimension(0, 0));
        triesLeftLabel.setPreferredSize(new java.awt.Dimension(200, 30));

        jLayeredPane1.setPreferredSize(new java.awt.Dimension(128, 203));

        platformLabel.setPreferredSize(new java.awt.Dimension(128, 203));
        platformLabel.setIcon(new javax.swing.ImageIcon("platform.png"));
        platformLabel.setBounds(0, 0, 220, 240);
        jLayeredPane1.add(platformLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);

        headLabel.setPreferredSize(new java.awt.Dimension(50, 53));
        headLabel.setPreferredSize(new java.awt.Dimension(50, 53));
        headLabel.setIcon(new javax.swing.ImageIcon("head2.png"));
        headLabel.setBounds(20, 50, 60, 60);
        jLayeredPane1.add(headLabel, javax.swing.JLayeredPane.PALETTE_LAYER);
        headLabel.setLocation(55, 15);
        headLabel.setVisible(false);

        bodyLabel.setPreferredSize(new java.awt.Dimension(8, 70));
        bodyLabel.setIcon(new javax.swing.ImageIcon("body.png"));
        bodyLabel.setBounds(30, 140, 8, 70);
        jLayeredPane1.add(bodyLabel, javax.swing.JLayeredPane.PALETTE_LAYER);
        bodyLabel.setLocation(75, 70);
        bodyLabel.setVisible(false);

        leftLegLabel.setPreferredSize(new java.awt.Dimension(40, 70));
        leftLegLabel.setIcon(new javax.swing.ImageIcon("leftLeg.png"));
        leftLegLabel.setBounds(40, 160, 40, 70);
        jLayeredPane1.add(leftLegLabel, javax.swing.JLayeredPane.PALETTE_LAYER);
        leftLegLabel.setLocation(39, 141);
        leftLegLabel.setVisible(false);

        rightLegLabel.setPreferredSize(new java.awt.Dimension(40, 70));
        rightLegLabel.setIcon(new javax.swing.ImageIcon("rightLeg.png"));
        rightLegLabel.setBounds(140, 110, 40, 70);
        jLayeredPane1.add(rightLegLabel, javax.swing.JLayeredPane.PALETTE_LAYER);
        rightLegLabel.setLocation(79, 141);
        rightLegLabel.setVisible(false);

        rightHandLabel.setPreferredSize(new java.awt.Dimension(28, 50));
        rightHandLabel.setIcon(new javax.swing.ImageIcon("rightHand.png"));
        rightHandLabel.setBounds(30, 140, 28, 50);
        jLayeredPane1.add(rightHandLabel, javax.swing.JLayeredPane.PALETTE_LAYER);
        rightHandLabel.setLocation(80, 80);
        rightHandLabel.setVisible(false);

        leftHandLabel.setIcon(new javax.swing.ImageIcon("leftHand.png"));
        leftHandLabel.setPreferredSize(new java.awt.Dimension(28, 50));
        leftHandLabel.setBounds(120, 140, 28, 50);
        jLayeredPane1.add(leftHandLabel, javax.swing.JLayeredPane.PALETTE_LAYER);
        leftHandLabel.setLocation(50, 80);
        leftHandLabel.setVisible(false);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(headerLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(mainPanelLayout.createSequentialGroup()
                                        .addComponent(sButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(tButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(mainPanelLayout.createSequentialGroup()
                                        .addComponent(yButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(zButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(15, 15, 15)
                                .addComponent(uButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(vButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(wButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(xButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addComponent(aButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(bButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(cButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(dButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(eButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(fButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(mainPanelLayout.createSequentialGroup()
                                        .addComponent(gButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(hButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(iButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(7, 7, 7))
                                    .addGroup(mainPanelLayout.createSequentialGroup()
                                        .addComponent(mButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(nButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12)
                                        .addComponent(oButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(pButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(mainPanelLayout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addComponent(kButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(lButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(mainPanelLayout.createSequentialGroup()
                                        .addGap(15, 15, 15)
                                        .addComponent(qButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(rButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(150, 150, 150))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(triesLeftLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(outputLabel))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(headerLabel)
                .addGap(33, 33, 33)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(eButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(fButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(aButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(gButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(hButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(iButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(qButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(oButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(xButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(wButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(vButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(yButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(zButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(2, 2, 2)
                .addComponent(outputLabel)
                .addGap(64, 64, 64)
                .addComponent(triesLeftLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        optionsMenu.setText("Options");

        highscoresMenuItem.setText("Check highscores");
        highscoresMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                highscoresMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(highscoresMenuItem);

        backMenuItem.setText("Back to main menu");
        backMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backMenuItemActionPerformed(evt);
            }
        });
        optionsMenu.add(backMenuItem);

        exitMenuItem.setText("Exit");
        optionsMenu.add(exitMenuItem);

        jMenuBar1.add(optionsMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(backgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 4, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 2, Short.MAX_VALUE)
                    .addComponent(backgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 2, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void wordInput(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wordInput

        if(triesLeftInt > 0) {

            Object source = evt.getSource();

            if(source == aButton) {
                System.out.println("a");
                checkWord('a');
                aButton.setEnabled(false);
            }
            else if(source == bButton) {
                System.out.println("b");
                checkWord('b');
                bButton.setEnabled(false);
            }
            else if(source == cButton) {
                System.out.println("c");
                checkWord('c');
                cButton.setEnabled(false);
            }
            else if(source == dButton) {
                System.out.println("d");
                checkWord('d');
                dButton.setEnabled(false);
            }
            else if(source == eButton) {
                System.out.println("e");
                checkWord('e');
                eButton.setEnabled(false);
            }
            else if(source == fButton) {
                System.out.println("f");
                checkWord('f');
                fButton.setEnabled(false);
            }
            else if(source == gButton) {
                System.out.println("g");
                checkWord('g');
                gButton.setEnabled(false);
            }
            else if(source == hButton) {
                System.out.println("h");
                checkWord('h');
                hButton.setEnabled(false);
            }
            else if(source == iButton) {
                System.out.println("i");
                checkWord('i');
                iButton.setEnabled(false);
            }
            else if(source == jButton) {
                System.out.println("j");
                checkWord('j');
                jButton.setEnabled(false);
            }
            else if(source == kButton) {
                System.out.println("k");
                checkWord('k');
                kButton.setEnabled(false);
            }
            else if(source == lButton) {
                System.out.println("l");
                checkWord('l');
                lButton.setEnabled(false);
            }
            else if(source == mButton) {
                System.out.println("m");
                checkWord('m');
                mButton.setEnabled(false);
            }
            else if(source == nButton) {
                System.out.println("n");
                checkWord('n');
                nButton.setEnabled(false);
            }
            else if(source == oButton) {
                System.out.println("o");
                checkWord('o');
                oButton.setEnabled(false);
            }
            else if(source == pButton) {
                System.out.println("p");
                checkWord('p');
                pButton.setEnabled(false);
            }
            else if(source == qButton) {
                System.out.println("q");
                checkWord('q');
                qButton.setEnabled(false);
            }
            else if(source == rButton) {
                System.out.println("r");
                checkWord('r');
                rButton.setEnabled(false);
            }
            else if(source == sButton) {
                System.out.println("s");
                checkWord('s');
                sButton.setEnabled(false);
            }
            else if(source == tButton) {
                System.out.println("t");
                checkWord('t');
                tButton.setEnabled(false);
            }
            else if(source == uButton) {
                System.out.println("u");
                checkWord('u');
                uButton.setEnabled(false);
            }
            else if(source == vButton) {
                System.out.println("v");
                checkWord('v');
                vButton.setEnabled(false);
            }
            else if(source == wButton) {
                System.out.println("w");
                checkWord('w');
                wButton.setEnabled(false);
            }
            else if(source == xButton) {
                System.out.println("x");
                checkWord('x');
                xButton.setEnabled(false);
            }
            else if(source == yButton) {
                System.out.println("y");
                checkWord('y');
                yButton.setEnabled(false);
            }
            else if(source == zButton) {
                System.out.println("z");
                checkWord('z');
                zButton.setEnabled(false);
            }
        }
        else {
            String message = "You have run out of tries!";
            JOptionPane.showMessageDialog(null, message, "Game over", JOptionPane.INFORMATION_MESSAGE);
        }

    }//GEN-LAST:event_wordInput

    private void highscoresMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_highscoresMenuItemActionPerformed
        Highscores highscores = new Highscores();
        highscores.setVisible(true);
        highscores.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_highscoresMenuItemActionPerformed

    private void backMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backMenuItemActionPerformed
        String message = "You are in the middle of a game. \nAre you sure you want to go to the main menu?";
        //int option = JOptionPane.showConfirmDialog(this, message, "")
        
        StartPage startPage = new StartPage();
        startPage.setVisible(true);
        
        this.dispose();
    }//GEN-LAST:event_backMenuItemActionPerformed

    public void checkWord(char c) {
               
        for (int i = 0; i < compWord.length(); i ++) {
            //System.out.println("outer for");
            char temp = compWord.charAt(i);
            
            if(temp == c) {
                System.out.println("match found at pos: " + i);
                //setOutputLabel.
                
                outputLabelText.replace(i, (i + 1), Character.toString(c));
                outputLabel.setText(outputLabelText.toString());
                correctLetter = true;
            } 
            
            if(!outputLabel.getText().contains("_")) { 
                
                ScoreService scoreServiceCall = new ScoreService();                
                HashMap<String, Integer> scoresInfo = scoreServiceCall.readScores();
                int userScore = triesLeftInt * 10;
                
                if(scoresInfo.get("lowestScore") == null) {
                    String message = "You have won! \nYour score is " + userScore + 
                                     " ..and it is a highscore! \nWould you like to save your score?";
                    int option = JOptionPane.showConfirmDialog(null, message, "Well done", JOptionPane.YES_NO_OPTION);
                    
                    //option: 1 = no; 0 = yes
                    
                        if(option == 1){
                            StartPage startPage = new StartPage();
                            startPage.setVisible(true);
                            this.dispose();

                        } else if(option == 0) {
                            SaveScore saveScore = new SaveScore(userScore);
                            saveScore.setVisible(true);
                            this.dispose();
                        }                   
                    
                } else {
                    if(scoresInfo.get("lowestScore") < userScore) {
                    String message = "You have won! \nYour score is " + userScore + 
                                     " ..and it is a highscore! \nWould you like to save your score?";
                    int option = JOptionPane.showConfirmDialog(null, message, "Well done", JOptionPane.YES_NO_OPTION);
                    
                    //option: 1 = no; 0 = yes
                    
                        if(option == 1){
                            StartPage startPage = new StartPage();
                            startPage.setVisible(true);
                            this.dispose();

                        } else if(option == 0) {
                            SaveScore saveScore = new SaveScore(userScore);
                            saveScore.setVisible(true);
                            this.dispose();
                        }
                    
                    } else {
                        String message = "You have won! \nYour score is " + userScore  + 
                                         "\nUnfortunately it isn't a highscore :(";
                        JOptionPane.showMessageDialog(null, message, "Well done", JOptionPane.OK_OPTION);

                    }
                }
            }
        }
        
        if(!correctLetter) {
                triesLeftInt--;
                System.out.println(triesLeftInt);
                triesLeftLabel.setText("Tries left: " + triesLeftInt);
                if(triesLeftInt == 5){
                    headLabel.setVisible(true);
                }
                if(triesLeftInt == 4){
                    bodyLabel.setVisible(true);
                }
                if(triesLeftInt == 3){
                    leftHandLabel.setVisible(true);
                }
                if(triesLeftInt == 2){
                    rightHandLabel.setVisible(true);
                }
                if(triesLeftInt == 1){
                    leftLegLabel.setVisible(true);
                }
                if(triesLeftInt == 0){
                    rightLegLabel.setVisible(true);
                }
        }
        
        correctLetter = false;
    }
    
    //main method
    public static void main(String args[]) {
        /*Set the Nimbus look and feel*/   //commented out
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HangmanGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HangmanGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HangmanGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HangmanGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new HangmanGUI().setVisible(true);
//            }
//        });
        
        HangmanGame newGame = new HangmanGame();
        
        //get content pane from main jfame to set the background colour
        Container cont = newGame.getContentPane();
        Color mainBackground = new Color(224, 255, 255);
        cont.setBackground(mainBackground);
        
        //set the visibility and other values
        newGame.setVisible(true);
    }
    

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton aButton;
    private javax.swing.JButton bButton;
    private javax.swing.JMenuItem backMenuItem;
    private javax.swing.JLabel backgroundLabel;
    private javax.swing.JLabel bodyLabel;
    private javax.swing.JButton cButton;
    private javax.swing.JButton dButton;
    private javax.swing.JButton eButton;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JButton fButton;
    private javax.swing.JButton gButton;
    private javax.swing.JButton hButton;
    private javax.swing.JLabel headLabel;
    private javax.swing.JLabel headerLabel;
    private javax.swing.JMenuItem highscoresMenuItem;
    private javax.swing.JButton iButton;
    private javax.swing.JButton jButton;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JButton kButton;
    private javax.swing.JButton lButton;
    private javax.swing.JLabel leftHandLabel;
    private javax.swing.JLabel leftLegLabel;
    private javax.swing.JButton mButton;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton nButton;
    private javax.swing.JButton oButton;
    private javax.swing.JMenu optionsMenu;
    private javax.swing.JLabel outputLabel;
    private javax.swing.JButton pButton;
    private javax.swing.JLabel platformLabel;
    private javax.swing.JButton qButton;
    private javax.swing.JButton rButton;
    private javax.swing.JLabel rightHandLabel;
    private javax.swing.JLabel rightLegLabel;
    private javax.swing.JButton sButton;
    private javax.swing.JButton tButton;
    private javax.swing.JLabel triesLeftLabel;
    private javax.swing.JButton uButton;
    private javax.swing.JButton vButton;
    private javax.swing.JButton wButton;
    private javax.swing.JButton xButton;
    private javax.swing.JButton yButton;
    private javax.swing.JButton zButton;
    // End of variables declaration//GEN-END:variables
}
