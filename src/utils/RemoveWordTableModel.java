/**
 * Authour: Deleepa
 * Date:    17/06/2013
 * Project: Hangman  |  RemoveWordTableModel
 */

package utils;

import java.util.ArrayList;
import java.util.Collections;
import javax.swing.table.AbstractTableModel;

public class RemoveWordTableModel extends AbstractTableModel {

    //the two variables, 'columnNames' and 'data' are automatically used to 
    //populate the JTable
    //populate these variabled in the constructor
    private String[] columnNames = {"Words in dictionary"};
    private Object[][] data;
    
    public RemoveWordTableModel(ArrayList<String> wordList) {
        wordList.removeAll(Collections.singleton(null)); 
        data = new Object[wordList.size()][1];
        
        for(int i = 0; i < wordList.size(); i++) {
            data[i][0] = wordList.get(i);            
        }
    }
    
    @Override
    public Object getValueAt(int row, int col){
        return data[row][col];
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return data.length;
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
}
