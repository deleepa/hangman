package utils;

import java.nio.file.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReadFile {
    
    public ReadFile() {
        
    }
    
    public ArrayList<String> readFromFile(String path) {
        //create file Path
        Path file = Paths.get(path);
        ArrayList<String> highscores = new ArrayList<String>();
        String temp = "";
        int index = 0;

        try
        {
            //create file input stream
            InputStream input = new BufferedInputStream(Files.newInputStream(file));

            //create reader (similar to Scanner for keyboard input)
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            
            //read the first line from the file
            temp = reader.readLine();
            highscores.add(temp);
//            System.out.println("first line temp: " + highscores.get(index));
            index++;
            
            //read the rest of the data in the file
            while (temp != null) {
                
                temp = reader.readLine();
//                System.out.println("temp: " + temp);
                highscores.add(temp);
                index++;
                
            }
            
            //close the reader
            reader.close();

        } catch(Exception e) {

            System.out.println("Message: " + e);
            
        } finally {
            
//            System.out.println("highscores.size(): " + highscores.size());
//            
//            for (int i = 0; i < highscores.size(); i ++) {
//                System.out.println(highscores.get(i));
//            }
            
            return highscores;
        }
    }

//    public static void main(String[] args) {
//        
//        ReadFile reader = new ReadFile();
//        reader.readFromFile("scores.txt");
//    }
}
