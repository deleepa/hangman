/*
 * Application: Hangman (Class: WriteToFile)
 * Authour:     Deleepa
 * Date:        25th Feb 2013
 */
package utils;

import java.nio.file.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;

public class WriteToFile {
    
    public WriteToFile() {

    }
    
    public void appendFile(String path, String[] writeData) throws Exception {
        
        Path filePath = Paths.get(path);
        
        try {
            // create output stream
            // Files.newOutputStream(filePath, CREATE, APPEND) == append to file, create new if doesnt exist
            OutputStream output = new BufferedOutputStream(Files.newOutputStream(filePath, CREATE, APPEND));

            //create writer (similar to System.out)
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));

            for(int i = 0; i < writeData.length ; i++) {
                // write each line to the file
                writer.write(writeData[i]);
                writer.newLine();
            }

            //close the file
            writer.close();
        }
        catch(Exception e) {
            
            System.out.println("Error: " + e);
            throw new Exception(WriteToFile.class.toString() + ".appendFile()");
            
        }
    }
    
    public void truncateAndWriteFile(String path, String[] writeData) throws Exception {
        Path filePath = Paths.get(path);
        
        try {
            // create output stream
            // Files.newOutputStream(filePath, CREATE, APPEND) == append to file, create new if doesnt exist
            OutputStream output = new BufferedOutputStream(Files.newOutputStream(filePath));

            //create writer (similar to System.out)
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));

            for(int i = 0; i < writeData.length ; i++) {
                // write each line to the file
                writer.write(writeData[i]);
                writer.newLine();
            }

            //close the file
            writer.close();
        }
        catch(Exception e) {
            
            System.out.println("Error: " + e);
            throw new Exception(WriteToFile.class.toString() + ".truncateAndWriteFile()");
            
        }
    }
}
